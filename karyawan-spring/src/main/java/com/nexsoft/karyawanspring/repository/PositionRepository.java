package com.nexsoft.karyawanspring.repository;

 import com.nexsoft.karyawanspring.entity.Position;
 import org.springframework.data.jpa.repository.JpaRepository;

public interface PositionRepository extends JpaRepository<Position, Integer> {

}
