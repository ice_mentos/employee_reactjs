package com.nexsoft.karyawanspring.controller;

import com.nexsoft.karyawanspring.entity.Position;
import com.nexsoft.karyawanspring.service.PositionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@CrossOrigin(origins = "*", allowedHeaders = "*")
@RestController
public class PositionController {
    @Autowired
    private PositionService service;

    @PostMapping("/addPosition")
    public Position addPosition(@RequestBody Position position) {
        return service.savePosition(position);
    }
}
