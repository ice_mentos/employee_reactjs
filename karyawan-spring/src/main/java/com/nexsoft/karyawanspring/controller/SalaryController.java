package com.nexsoft.karyawanspring.controller;

import com.nexsoft.karyawanspring.entity.Position;
import com.nexsoft.karyawanspring.entity.Salary;
import com.nexsoft.karyawanspring.service.SalaryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@CrossOrigin(origins = "*", allowedHeaders = "*")
@RestController
public class SalaryController {
    @Autowired
    private SalaryService service;

    @PostMapping("/addSalary")
    public Salary addSalary(@RequestBody Salary salary) {
        return service.saveSalary(salary);
    }
}
