package com.nexsoft.karyawanspring.service;


import com.nexsoft.karyawanspring.entity.Salary;
import com.nexsoft.karyawanspring.repository.SalaryRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class SalaryService {
    @Autowired
    private SalaryRepository repository;

    public Salary saveSalary(Salary salary) {
        return repository.save(salary);
    }
}