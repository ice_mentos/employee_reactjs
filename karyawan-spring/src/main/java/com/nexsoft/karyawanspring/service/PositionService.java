package com.nexsoft.karyawanspring.service;

import com.nexsoft.karyawanspring.entity.Employee;
import com.nexsoft.karyawanspring.entity.Position;
import com.nexsoft.karyawanspring.repository.EmployeeRepository;
import com.nexsoft.karyawanspring.repository.PositionRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class PositionService {
    @Autowired
    private PositionRepository repository;

    public Position savePosition(Position position) {
        return repository.save(position);
    }

}
