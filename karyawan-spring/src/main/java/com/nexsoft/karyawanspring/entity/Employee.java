package com.nexsoft.karyawanspring.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "data")
public class Employee {
    @Id
    private int nik;
    @Column(nullable = false)
    private String nama;
    @Column(nullable = false)
    private String tempat_tgl;
    @Column(nullable = false)
    private String jenis_kelamin;
    @Column(nullable = false)
    private String alamat;
    @Column(nullable = false)
    private String rt_rw;
    @Column(nullable = false)
    private String kel_desa;
    @Column(nullable = false)
    private String kecamatan;
    @Column(nullable = false)
    private String agama;
    @Column(nullable = false)
    private String status;
    @Column(nullable = false)
    private String pekerjaan;
    @Column(nullable = false)
    private String kewarganegaraan;

    @JsonIgnore
    @OneToOne(mappedBy="employee")
    private Position position;

    @JsonIgnore
    @OneToOne(mappedBy="employee")
    private Salary salary;
}